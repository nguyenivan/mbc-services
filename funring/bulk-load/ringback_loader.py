from google.appengine.tools import bulkloader
from google.appengine.ext import db

class RingBack(db.Model):
   RingBackID = db.StringProperty()    
   Code    = db.StringProperty()
   WapCode = db.StringProperty()    
   VTCode = db.StringProperty()    
   VNPCode = db.StringProperty()    
   VMSCode = db.StringProperty()    
   Provider = db.StringProperty()    
   RingBackName    = db.StringProperty()
   ImagePath = db.StringProperty()    
   ImagePathWeb = db.StringProperty()     
   Size    = db.StringProperty()
   Length = db.StringProperty()    
   BitRate = db.StringProperty()    
   MP3Path = db.StringProperty()    
   Albums = db.StringProperty()    
   Singers = db.StringProperty()    
   SongWriters = db.StringProperty()    
   CateID = db.StringProperty()    
   CateName = db.StringProperty()    
   CreateDate = db.StringProperty()    
   CreateBy = db.StringProperty()    
   CreateByName    = db.StringProperty()
   ActiveDate = db.StringProperty()    
   ActiveBy = db.StringProperty()    
   UpdateDate = db.StringProperty()    
   UpdateBy = db.StringProperty()    
   Priority = db.StringProperty()    
   TotalView = db.StringProperty()    
   Rating = db.StringProperty()    
   Price = db.StringProperty()    
   SendPrice = db.StringProperty()    
   IsActive = db.StringProperty()    
   TotalDownload = db.StringProperty()
   
class RingBackLoader(bulkloader.Loader):
    def __init__(self):
        fields = [
        ("RingBackID",lambda x: x.decode('utf-8')),
		("Code",lambda x: x.decode('utf-8')),
		("WapCode",lambda x: x.decode('utf-8')),
		("VTCode",lambda x: x.decode('utf-8')),
		("VNPCode",lambda x: x.decode('utf-8')),
		("VMSCode",lambda x: x.decode('utf-8')),
		("Provider",lambda x: x.decode('utf-8')),
		("RingBackName",lambda x: x.decode('utf-8')),
		("ImagePath",lambda x: x.decode('utf-8')),
		("ImagePathWeb",lambda x: x.decode('utf-8')), 	
		("Size",lambda x: x.decode('utf-8')),
		("Length",lambda x: x.decode('utf-8')),
		("BitRate",lambda x: x.decode('utf-8')),
		("MP3Path",lambda x: x.decode('utf-8')),
		("Albums",lambda x: x.decode('utf-8')),
		("Singers",lambda x: x.decode('utf-8')),
		("SongWriters",lambda x: x.decode('utf-8')),
		("CateID",lambda x: x.decode('utf-8')),
		("CateName",lambda x: x.decode('utf-8')),
		("CreateDate",lambda x: x.decode('utf-8')),
		("CreateBy",lambda x: x.decode('utf-8')),
		("CreateByName",lambda x: x.decode('utf-8')),
		("ActiveDate",lambda x: x.decode('utf-8')),
		("ActiveBy",lambda x: x.decode('utf-8')),
		("UpdateDate",lambda x: x.decode('utf-8')),
		("UpdateBy",lambda x: x.decode('utf-8')),
		("Priority",lambda x: x.decode('utf-8')),
		("TotalView",lambda x: x.decode('utf-8')),
		("Rating",lambda x: x.decode('utf-8')),
		("Price",lambda x: x.decode('utf-8')),
		("SendPrice",lambda x: x.decode('utf-8')),
		("IsActive",lambda x: x.decode('utf-8')),
		("TotalDownload",lambda x: x.decode('utf-8'))
        ]
        
        bulkloader.Loader.__init__(self, "RingBack", fields)

loaders = [RingBackLoader]

#if __name__ == "__main__":
#    bulkload.main(RingBackLoader())
