from google.appengine.ext import db

class RingBack(db.Model):
   RingBackID = db.StringProperty()	
   Code	= db.StringProperty()
   WapCode = db.StringProperty()	
   VTCode = db.StringProperty()	
   VNPCode = db.StringProperty()	
   VMSCode = db.StringProperty()	
   Provider = db.StringProperty()	
   RingBackName	= db.StringProperty()
   ImagePath = db.StringProperty()	
   ImagePathWeb = db.StringProperty() 	
   Size	= db.StringProperty()
   Length = db.StringProperty()	
   BitRate = db.StringProperty()	
   MP3Path = db.StringProperty()	
   Albums = db.StringProperty()	
   Singers = db.StringProperty()	
   SongWriters = db.StringProperty()	
   CateID = db.StringProperty()	
   CateName = db.StringProperty()	
   CreateDate = db.StringProperty()	
   CreateBy = db.StringProperty()	
   CreateByName	= db.StringProperty()
   ActiveDate = db.StringProperty()	
   ActiveBy = db.StringProperty()	
   UpdateDate = db.StringProperty()	
   UpdateBy = db.StringProperty()	
   Priority = db.StringProperty()	
   TotalView = db.StringProperty()	
   Rating = db.StringProperty()	
   Price = db.StringProperty()	
   SendPrice = db.StringProperty()	
   IsActive = db.StringProperty()	
   TotalDownload = db.StringProperty()

class Genre(db.Model):
	cate_id = db.StringProperty()
	name = db.StringProperty()
	name_lower_case = db.StringProperty()

class Artist(db.Model):
	name = db.StringProperty()
	name_lower_case = db.StringProperty()
	
# This class is for matching the authorization code with phone number and telco provider
class RingBackAuthorization(db.Model):
	auth_code = db.StringProperty()
	phone_number = db.StringProperty()
	#VT , VNP or VMS
	telco = db.StringProperty()

class RingBackSetRecord(db.Model):
	set_date = db.DateTimeProperty(auto_now_add=True)
	auth_code = db.StringProperty()
	telco = db.StringProperty()
	song_id = db.IntegerProperty()
	song_name = db.StringProperty()

