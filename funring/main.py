#!/usr/bin/env python

import cgi
import datetime
import wsgiref.handlers
import os
from google.appengine.ext import db, webapp
from django.utils import simplejson
from django.utils.html import strip_tags
from google.appengine.api import memcache

from funring.models import *

class Utils:
	@staticmethod
	def get_offset(offset):
		if offset < 0:
			offset = 0
		return offset
	
	@staticmethod
	def get_limit(limit):
		if limit > 20:
			limit = 20
		elif limit < 0:
			limit=10
		return limit
	
class GetGenres(webapp.RequestHandler):

	# Get a dictionary of unique categories and sort by CateID
	# TODO: Memcache this is a MUST
	@staticmethod
	def get_unique():

		def get_unique(existing_dict, new_array):
			result = existing_dict
			for obj in new_array:
				if obj.CateID not in result:
					result[obj.CateID] = obj.CateName
			return result

		i = 0
		rings = []
		result_dict = dict()
		while rings or i == 0:
			rings = RingBack.all().fetch(offset=i*1000,limit=1000)
			if rings:
				result_dict = get_unique(result_dict, rings)
			i += 1
		return result_dict

	# Calculate the key for this kind
	def get_key(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))
		# Format of key is genre_offset_limit
		key = "genre_%d" % (offset,limit).__hash__()
		return key
	
	# Query Genre data from data store if no memcache hit
	def query_data(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))
		
		json_data = []
		genres = Genre.all().fetch(offset=offset,limit=limit)
		for genre in genres:
			json_data.extend([{'id':genre.key().id(),
								'name': genre.name
								}])
		return json_data		
	
	# Try to get data from memcache
	def get_data(self):
		key = self.get_key()
		data = memcache.get(key)
		if data is not None:
			return data
		else:
			data = self.query_data()
			# Expiry: 1 month
			memcache.add(key,data,2592000)
			return data
		
	def get(self):
		data = self.get_data()								
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(data))

class GetArtists(webapp.RequestHandler):


	# Get a dictionary of unique categories and sort by CateID
	# TODO: Memcache this is a MUST
	@staticmethod
	def get_unique():

		def get_unique(existing_dict, new_array):
			result = existing_dict
			for obj in new_array:
				if obj.Singers not in result.values():
					result[obj.key ().id()] = obj.Singers
			return result

		i = 0
		rings = []
		result_dict = dict()
		while rings or i == 0:
			rings = RingBack.all().fetch(offset=i*1000,limit=1000)
			if rings:
				result_dict = get_unique(result_dict, rings)
			i += 1
		return result_dict
	
	# Calculate the key for this kind
	def get_key(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))
		# Format of key is genre_offset_limit
		key = "artist_%d" % (offset,limit).__hash__()
		return key
	
	# Query Genre data from data store if no memcache hit
	def query_data(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))
		
		json_data = []
		artists = Artist.all().fetch(offset=offset,limit=limit)
		for artist in artists:
			json_data.extend([{'id':artist.key().id(),
								'name':artist.name
								}])

		return json_data		
	
	# Try to get data from memcache
	def get_data(self):
		key = self.get_key()
		data = memcache.get(key)
		if data is not None:
			return data
		else:
			data = self.query_data()
			# Expiry: 1 month
			memcache.add(key,data,2592000)
			return data
		
	def get(self):
		data = self.get_data()								
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(data))

class GetSongs(webapp.RequestHandler):
	
	# Calculate the key for this kind
	def get_key(self):
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))
		genre_id =  int(values.get('genre_id', -1))
		artist_id =  int(values.get('artist_id', -1))
		telco = values.get('telco')
		# Format of key is genre_offset_limit
		key = "song_%d" % (telco,genre_id,artist_id,offset,limit).__hash__()
		return key
	
	# Query Genre data from data store if no memcache hit
	def query_data(self):
						
		values = dict([(k, self.request.get(k)) for k in self.request.arguments()])
		offset = int(values.get('offset',0))
		limit = int(values.get('limit',10))
		genre_id =  int(values.get('genre_id', -1))
		artist_id =  int(values.get('artist_id', -1))
		telco = values.get('telco') # Can be VT, VNP or VMS
		artists = GetArtists.get_unique()
		genre = Genre.get_by_id(genre_id)
		artist = Artist.get_by_id(artist_id)
		offset = Utils.get_offset(int(values.get('offset',0)))
		limit = Utils.get_limit(int(values.get('limit',10)))

		query = RingBack.all()
		if genre:
			query = query.filter("CateID =", genre.cate_id)
		if artist:
			query = query.filter("Singers =", artist.name)

		codeField = ''
		if telco == 'VT':
			codeField = "VTCode"
		elif telco == "VNP":
			codeField = "VNPCode"
		elif telco == "VMS":
			codeField = "VMSCode"
		query = query.filter("%s !=" % codeField,'')

		rings = query.fetch(offset=offset,limit=limit)
		json_data = []
		for ring in rings:
			json_data.extend([{'id':ring.key().id(), 
								'name':ring.RingBackName,
								'code':getattr(ring, codeField)
								}])

		return json_data		
	
	# Try to get data from memcache
	def get_data(self):
		key = self.get_key()
		data = memcache.get(key)
		if data is not None:
			return data
		else:
			data = self.query_data()
			# Expiry: 1 month
			memcache.add(key,data,2592000)
			return data
		
	def get(self):		
		data = self.get_data()		
		self.response.headers['Content-Type'] = 'application/json'
		self.response.out.write(simplejson.dumps(data))

class SetTone(webapp.RequestHandler):
	# TODO: Record the set ring back tone.
	def get(self):
		pass

class MainPage(webapp.RequestHandler):
	# TODO: Record the set ring back tone.
	def get(self):
		pass

class SyncMetadata(webapp.RequestHandler):
	def get(self):
		genres = GetGenres.get_unique()
		self.response.out.write("<b>Syncing Genres ... </b><br/>")
		# Update Genres
		for id in genres:
			name_lower_case = genres[id].lower()
			if not Genre.all().filter('name_lower_case =', name_lower_case).fetch(1): #the genre is not existing in the list
				newGenre = Genre()
				newGenre.cate_id = id
				newGenre.name = genres[id]
				newGenre.name_lower_case = name_lower_case
				newGenre.put()
		self.response.out.write("<b>Done!</b></br>")

		self.response.out.write("<b>Syncing Artists ... </b></br>")		
		# Update Artists
		artists = GetArtists.get_unique()
		for id in artists:
			name_lower_case = artists[id].lower()
			if not Artist.all().filter('name_lower_case =', name_lower_case).fetch(1): #the genre is not existing in the list
				newArtist = Artist()
				newArtist.name = artists[id]
				newArtist.name_lower_case = name_lower_case
				newArtist.put()
		self.response.out.write("<b>Done!</b></br>")

application = webapp.WSGIApplication([
	('/funring/get_genres', GetGenres),
	('/funring/get_artists', GetArtists),
	('/funring/get_songs', GetSongs),
	('/funring/set_tone', SetTone),
	('/funring/sync_metadata', SyncMetadata)
], debug=True)


def main():
	wsgiref.handlers.CGIHandler().run(application)

if __name__ == '__main__':
	main()

